from kivy.app import App
from mmgs108.api.http.session import Session
from mmgs108.api.http.vlan_table import VLanTable

from mmgs108.api.http.sap import Sap
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.uix.label import Label
from kivy.uix.scrollview import ScrollView
from kivy.uix.gridlayout import GridLayout
from kivy.core.window import Window


class Gs108App(App):

    def __init__(self, ips, **kwargs):
        super().__init__(**kwargs)
        self.__switches = {}
        self.__ips = ips

    def stop(self, *largs):
        for key, switch in self.__switches.items():
            switch["session"].close()

    def build(self):

        for ip in self.__ips:
            sap = Sap(ip)
            session = Session(sap, "password")
            table = VLanTable(session)
            self.__switches[ip] = {
                "sap": sap,
                "session": session,
                "table": table
            }

        grid = GridLayout(cols=1, spacing=10, size_hint_y=None)
        grid.bind(minimum_height=grid.setter('height'))
        for name, switch in self.__switches.items():
            grid.add_widget(self.build_switch(name, switch["table"]))

        root = ScrollView(size_hint=(1, None), size=(Window.width, Window.height))
        root.add_widget(grid)
        return root

    def build_vtable(self, table):
        layout = BoxLayout()
        for key, value in table.items():
            tmp_layout = BoxLayout(orientation='vertical')
            tmp_layout.add_widget(Label(text='{}:'.format(key), size_hint=(1, None), height=30))
            vlan_input = VLanIdInput(table, key, text='{}'.format(value), multiline=False, size_hint=(1, None), height=30)
            tmp_layout.add_widget(vlan_input)
            layout.add_widget(tmp_layout)

        return layout

    def build_switch(self, switch, table):
        layout = GridLayout(cols=1, spacing=10, size_hint_y=None)
        layout.add_widget(Label(text='{}:'.format(switch), size_hint=(1, None), height=30))
        layout.add_widget(self.build_vtable(table))
        return layout


class VLanIdInput(TextInput):

    def __init__(self, table, port, **kwargs):
        super().__init__(**kwargs)
        self.__table = table
        self.__port = port

    def on_text_validate(self):
        self.__table[self.__port] = self.text
