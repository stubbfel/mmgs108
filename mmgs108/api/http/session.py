from requests import session


class Session:
    __LOGIN_PATH = "login.cgi"
    __LOGOUT_PATH = "logout.cgi"
    __PASSWORD_POST_KEY = "password"

    def __del__(self):
        self.close()

    def close(self):
        self.__http_session.get(self.__logout_url)
        self.__http_session.close()

    def __init__(self, sap, password):
        self.__api = sap
        self.__login_url = sap.generate_url(Session.__LOGIN_PATH)
        self.__logout_url = sap.generate_url(Session.__LOGOUT_PATH)
        self.__http_session = session()
        self.__http_session.post(self.__login_url, data={Session.__PASSWORD_POST_KEY: password})

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        return self

    def get(self, path):
        return self.__http_session.get(self.__api.generate_url(path))

    def post(self, path, data):
        return self.__http_session.post(self.__api.generate_url(path), data=data)
