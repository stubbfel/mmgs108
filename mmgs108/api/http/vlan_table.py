import re


class VLanTable:
    __Q_BASIC = "8021qBasic"
    __Q_BASIC_HTM = "{}.htm".format(__Q_BASIC)
    __Q_BASIC_CGI = "{}.cgi".format(__Q_BASIC)

    def __init__(self, session):
        self.__session = session
        self.__vlan_table, self.__hash_value = self.__read_table()

    def __read_table(self):
            response = self.__session.get(VLanTable.__Q_BASIC_HTM)
            html = response.text
            table = VLanTable.__html_table_to_dict(html)
            hash_value = VLanTable.__get_hash_from_html_table(html)
            return table, hash_value

    def __str__(self):
            return str(self.__vlan_table)

    def __getitem__(self, item):
        return self.__vlan_table[item]

    def __setitem__(self, key, value):
        self.__vlan_table[key] = str(value)
        self.flush_table()

    def flush_table(self):
        data = VLanTable._create_post_param(self.__vlan_table, self.__hash_value)
        self.__session.post(VLanTable.__Q_BASIC_CGI, data)
        self.__vlan_table, self.__hash_value = self.__read_table()

    def items(self):
        return self.__vlan_table.items()

    @staticmethod
    def _create_post_param(table, hash_value):
        params = {
            "hash": hash_value,
            "hiddVlan": "",
            "status": "Enable",
        }

        return {**params, **table}

    def to_html_table(self):
        return VLanTable.__dict_to_html_table(self.__vlan_table)

    @staticmethod
    def __html_table_to_dict(html):
        pattern = re.compile("name='(?P<port_name>port[0-9]+)' value='(?P<port_vlan_id>(all|[0-9]+))'")
        match_iter = pattern.finditer(html)
        vlan_table = {}
        for match in match_iter:
            vlan_table[match["port_name"]] = match["port_vlan_id"]

        return vlan_table

    @staticmethod
    def __get_hash_from_html_table(html):
        pattern = re.compile("name='hash' id='hash' value='(?P<hash_value>[0-9]+)'")
        return pattern.search(html)["hash_value"]

    @staticmethod
    def __dict_to_html_table(table):
        header = ""
        values = ""
        for key, value in table.items():
            header = header + "<th>{}</th>\n".format(key)
            values = values + "<td><input type='text' name='{}' value='{}' size='11' maxlength='4'></td>\n".format(key, value)

        return "<table><tr>{} </tr><tr>{}</tr><table>".format(header, values)
