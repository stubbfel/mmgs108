from mmgs108.api.http.session import Session


class Sap:

    def __init__(self, base_address):
        self.__address = base_address

    def execute_request(self, request, password):
        with Session(self.__address, password) as apiSession:
            apiSession.get("foo")

    def generate_url(self, path):
        return "http://{}/{}".format(self.__address, path)