import unittest
import random
from mmgs108.api.http.sap import Sap
from mmgs108.api.http.session import Session
from mmgs108.api.http.vlan_table import VLanTable


class TestVLanTable(unittest.TestCase):

    def test_get_vlan_table(self):
        sap = Sap("10.0.0.254")
        vlan_id = random.randrange(1, 4095)
        port_key = "port{}".format(random.randrange(2, 8))
        with Session(sap, "password") as session1:
            table = VLanTable(session1)
            table[port_key] = vlan_id
            self.assertEqual(str(vlan_id), table[port_key])

        with Session(sap, "password") as session2:
            table = VLanTable(session2)
            self.assertEqual(str(vlan_id), table[port_key])


if __name__ == '__main__':
    unittest.main()
