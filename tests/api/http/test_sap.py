import unittest
from mmgs108.api.http.sap import Sap
from mmgs108.ui.kivy.app import Gs108App


class TestSap(unittest.TestCase):
    def test_generate_url(self):
        sap = Sap("foo")
        self.assertEqual("http://foo/bar", sap.generate_url("bar"))

    def test_runServer(self):
        Gs108App(["10.0.0.254"]).run()


if __name__ == '__main__':
    unittest.main()
